// number guessing game
// The program prompts the user to choose the range of the random number.
// It generates a random number within the chosen range using the Go standard library's math/rand package.
// The user is prompted to guess the number, and the program provides feedback whether the guess is too high or too low.
// The user has a maximum of 5 attempts to guess the correct number, and the program keeps track of the number of attempts made.
// If the user guesses the correct number within the limit of attempts, the program prints a congratulatory message and the user's score.
//  The score is initialized to 5 and decreases by 1 for each wrong guess.
// If the user reaches the limit of attempts without guessing correctly, the program prints a message indicating that the user has lost.

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// Seed the random number generator
	rand.Seed(time.Now().Unix())

	// Prompt the user to choose the range of the random number
	fmt.Println("Welcome to the number guessing game!")
	fmt.Println("Choose the range of the random number:")

	var min, max int
	fmt.Print("Minimum number: ")
	fmt.Scanln(&min)
	fmt.Print("Maximum number: ")
	fmt.Scanln(&max)

	// Generate a random number within the range
	number := rand.Intn(max-min+1) + min
	fmt.Printf("A random number between %d and %d has been generated.\n", min, max)

	// Initialize the number of attempts
	attempts := 0
    score := 5

	// Loop until the user guesses correctly or reaches the limit of attempts
	for attempts < 5 {
		// Prompt the user to guess the number
		fmt.Println("Guess the number:")

		// Read the user's input
		var guess int
		fmt.Scanln(&guess)

		// Increment the number of attempts
		attempts++

		// Check if the guess is correct
		if guess == number {
			fmt.Printf("Congratulations! You guessed the number %d in %d attempts.🥳 And your score is %d \n", number, attempts,score)
			break
		} else {
			// Provide a hint based on whether the guess is too high or too low
			if guess < number {
				fmt.Println("Too low, try again.")
                score -=1
			} else {
				fmt.Println("Too high, try again.")
                score -=1
			}
		}
	}

	// If the user reaches the limit of attempts without guessing correctly
	if attempts == 5 {
		fmt.Printf("Sorry, you did not guess the number %d within the limit of %d attempts.\n So, You lose", number, attempts)
	}
}


